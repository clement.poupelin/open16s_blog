---
title: "3 : Diversity Analysis"
author: "Clément Poupelin"
date: "2024-05-29"
format: 
  html:
    embed-resources: false
    toc: true
    code-fold: true
    code-summary: "Show the code"
    code-tools: true
    toc-location: right
    page-layout: article
    code-overflow: wrap
toc: true
number-sections: false
editor: visual
categories: [code, analysis, Open16s]
image: "diversity.jpg"
description: "Diversity analysis on data from the INRAE Open16s project"
---

## Introduction

The aim of this document is to propose a diversity analysis for the Open16S project data samples.

## Theory

Diversity, or richness, refers to the number of different taxa (ASV, species, etc.), counted or estimated, in a system or sample studied (for ease of understanding, we often use the term species rather than taxon). To estimate diversity, we make certain assumptions. First, the systems from which the samples come are known and the taxonomy of the different species can be identified. Then, systems are considered equidistant in the sense that if we add a species into a system, this will correspond to an increase in diversity of one unit. It will therefore not depend on whether the species is close to others or not (unless we use specific distances based on phylogeny).

In microbial ecology, two main types of diversity analyzes are carried out depending on the biological questions asked:

-     $\boldsymbol{\alpha}$-diversity: it corresponds to the diversity in a uniform system of fixed size. In our context, this means that we look at the diversity of species in each of the samples.

-     $\boldsymbol{\beta}$-diversity: it measures how different local systems are. In our context, this means that we will look at whether different samples have close diversity or not.

These measurements are therefore very important in microbial analyses, because they make it possible to identify the factors that influence microbial diversity and to understand the dynamics of microbial communities. We can then ask ourselves the question of whether the data from the Open16S project resulting from an open science approach makes it possible to find the diversity characteristics specific to each biotope even if the samples come from independent studies and different biological materials.

## Setup

```{r, message=FALSE}
library(tidyverse)  ## data manipulation
library(phyloseq)   ## analysis of microbiome census data
library(ape)        ## for tree manipulation
library(vegan)      ## for community ecology analyses
library(phyloseq.extended)
library(scales)
library(kableExtra)
library(gridExtra)

library(MiscMetabar)
# install.packages("devtools")
# devtools::install_github("adw96/breakaway")
library(breakaway)
```

## Import Data

```{r}
## without rarefaction
df_final <- readRDS("~/Documents/open16s/Data/df_final.rds")

## with rarefaction
# df_final_rarefied <- readRDS("~/Documents/open16s/Data/df_final_rarefied.rds")
df_final_rarefied <- readRDS("~/Documents/open16s/Data/df_final_rarefied2000.rds")
# df_final_rarefied <- readRDS("~/Documents/open16s/Data/df_final_rarefied10000.rds")
df_final_rarefied
```

We carry out the analyzes on our dataset modified by the previous quarto. Indeed, the dataset we use is undergoing rarefaction ([1: Rarefaction study](../posts/1_Open16s_rarefaction_study.qmd)). We just have to study diversity.

## Alpha-Diversity

As a reminder, the aim of $\alpha-$diversity is to study the diversity present within a community (intra-sample diversity). We will use here 4 diversity indices:

-    **Observed**: this is the number of species observed

-     **Chao1**: this is the number of species observed + estimate of the number of species not observed

-     **Shannon entropy**: this is the width of the distribution of the relative abundance of the species

-     **Inverse Simpson**: it is the inverse of the probability that two bacteria taken at random belong to the same species

Also, we will test the possibility of estimating diversity without rarefaction via the breakaway package (analysis done for the Biotope variable).

::: panel-tabset
### Biotope

::: panel-tabset
#### with rarefaction

Here we look at the diversity present in each of our Biotopes. First, let's take a look at our data according to this variable.

```{r}
table(sample_data(df_final_rarefied)$Biotope)
```

Digester and food are our “most represented” Biotopes. These are the ones where we have the most samples.

```{r, fig.width=18, fig.height=10}
pr <- plot_richness(physeq = df_final_rarefied, 
              x = "Biotope",              # variable to map to the horizontal axis
              color = "Biotope",
              measures = c("Observed", "Chao1", "Shannon", "InvSimpson")) + 
  geom_boxplot(aes(group = Biotope))+
    theme(axis.text.x = element_text(size = 20, hjust = 0.5))
print(pr)
```

```{r, fig.width=18, fig.height=10}
pr <- plot_richness(physeq = df_final_rarefied, 
              x = "Biotope",              # variable to map to the horizontal axis
              color = "Biotope",
              measures = c("Observed", "Chao1", "Shannon", "InvSimpson")) + 
  geom_boxplot(aes(group = Biotope, fill = Biotope), color = "black", alpha = 0.4, outlier.shape = NA) +  # Add boxplot, hide outliers
  geom_jitter(width = 0.2, size = 1.5, alpha = 0.3) +  # Add points
  # theme_classic() +  # Set background to white
  theme(legend.key.size = unit(2, "lines"),
        legend.text = element_text(size = 20),
        legend.title = element_text(size = 20)) +
  theme(axis.text.x = element_text(size = 10, hjust = 0.5))
print(pr)
```

We see a greater value of richness at the level of digester, human_gut and cow than for food. But we also notice that food has many more samples than the other Biotope.

Small point of interpretation: the very first boxplot shows us the diversity of OTU/ASV or species observed in the different samples associated with the Cow Biotope.



```{r, fig.width=25, fig.height=25}
pr <- pr + ggtitle("Mesures d'alpha diversité après rarefaction") + 
  labs(y = "") +  
  labs(x = "") +  
  theme_bw() +
  theme(
    legend.key.size = unit(2, "lines"),
    legend.text = element_text(size = 40),
    legend.title = element_text(size = 50),
    axis.text.x = element_blank(), 
    axis.title.y = element_blank(),
    axis.text.y = element_text(size = 30), 
    plot.title = element_text(size = 40, hjust = 0.5, face = "bold", margin = margin(t = 10, b = 10)),
    strip.text = element_text(size = 30)  # Augmenter la taille du texte des titres des sous-graphes
  )

pr
```

#### without rarefaction

Let's look at what the wealth estimated by breakaway gives us

```{r, message=FALSE, warning=FALSE}
breakaway_richness <- df_final %>% breakaway
breakaway_richness[1] %>% str
```

```{r, fig.width=40, fig.height=20, message=FALSE, warning=FALSE}
# plot(breakaway_richness, physeq=df_final, color="Biotope")
```

```{r}
library(magrittr) # pour %$%
s <- summary(breakaway_richness) %>% as_tibble 
s
s %$% model %>% table
# table(s$sample_names, s$model)
```

The different models used. depending on the models may have studs

let's make a sub-dataset with the "without estimates"

```{r}
subset_data <- s %>%
  filter(is.nan(lower) & is.nan(upper))
subset_data
print(paste("On a ",dim(subset_data)[1],"samples où estimate sans sd"))
```

```{r}
# plot(breakaway(frequencytablelist[[1]])) # voir plus tard que sur 22 il y a eu une estimation
```

The real problem with our data when using breakaway is that we are mostly on samples where the species are seen several times but not the same number of times. So a lot of species that function as singletons in our samples. This may come from the fact that our data mainly has a lot of reads so we can say that almost everything is observed to such an extent that we cannot estimate the unobserved. And if we return to the rarefaction curve, even the samples with little read seemed to reach a plateau which meant that few or no other species were discovered (given our data). With a lower rarefaction threshold, the estimate would be more different (like Chao1).


```{r}
frequencytablelist <- build_frequency_count_tables(otu_table(df_final))
head(frequencytablelist[[1]])
table(frequencytablelist[[1]]$Freq)
```

So on this sample, we have 1 species observed only 13 times, 1 species observed only 17 times etc....

```{r}
table(otu_table(df_final)[,1])
length(otu_table(df_final)[,1]) - 3130
```

So we have 36 features observed in this sample. So there was no estimate.

The problem is that we only have frequencies of 1 on the first (and other) samples. All of that is too low to make a breakaway and therefore no estimate is made on these samples.

```{r}
breakaway(frequencytablelist[[1]])
```

```{r}
test1 <- kemp(frequencytablelist[[1]])
test1
```

```{r}
sample_sums(df_final)[1]
sample_richness(df_final)[1]
frequencytablelist[[1]]$Var1
sum(frequencytablelist[[1]]$Var1)
```

```{r}
sample_sums(df_final)[1]
```

```{r, message=TRUE, warning=FALSE}
breakaway(df_final)[1]
```

on the other hand on the rarified game it works!!! But still full of warnings and messages so on one hand it's okay

```{r, message=TRUE, warning=FALSE}
breakaway(df_final_rarefied)[1]
```

```{r}
sample <- 1 # we choose a random sample
frequencytablelist2 <- build_frequency_count_tables(otu_table(df_final_rarefied))
head(frequencytablelist2[[1]])

```

On voit effectivement qu'il n'y a pas que des 1

exemple avec estimation

```{r, message=TRUE, warning=FALSE}
breakaway(df_final)[22]
as_tibble(summary(breakaway_richness))[22,]
head(frequencytablelist[[22]])
```

We see here that there are not only 1s (we have 3 species observed only 1 time, etc...)

```{r, fig.width=15, fig.height=20}
sample_data(df_final)$Estimated_richness <- summary(breakaway_richness)$estimate
p <- ggplot(sample_data(df_final), aes(x=Biotope, y=Estimated_richness, color = Biotope)) + 
  geom_boxplot(aes(group = Biotope, fill = Biotope), color = "black", alpha = 0.4, outlier.shape = NA) +  # Add boxplot, hide outliers
  geom_jitter(width = 0.2, size = 1.5, alpha = 0.3) +  # Add points
  ggtitle("Richesse observée estimée avec Breakaway") + 
  labs(y = "") +  
  labs(x = "") +  
  theme_bw() +
  theme(
    legend.position = "none",
    legend.key.size = unit(2, "lines"),
    legend.text = element_text(size = 40),
    legend.title = element_text(size = 50),
    axis.text.x = element_blank(), 
    axis.title.y = element_text(size = 40),
    axis.text.y = element_text(size = 30), 
    plot.title = element_text(size = 40, hjust = 0.5, face = "bold", margin = margin(t = 10, b = 10)),
    strip.text = element_text(size = 30)
  )
p
```

```{r}

# Ajout des IC sur les points qui en ont


# # Ajouter les estimations et les intervalles de confiance à df_final
# sample_data(df_final)$Estimated_richness <- summary(breakaway_richness)$estimate
# sample_data(df_final)$Lower_CI <- summary(breakaway_richness)$lower
# sample_data(df_final)$Upper_CI <- summary(breakaway_richness)$upper
# 
# # Créer le graphique avec ggplot2
# p <- ggplot(df_final, aes(x=Biotope, y=Estimated_richness, color = Biotope)) + 
#   geom_boxplot() +
#   geom_errorbar(aes(ymin = Lower_CI, ymax = Upper_CI), width = 0.2) +
#   theme_minimal() + 
#   labs(title = "Estimated Richness with Confidence Intervals",
#        x = "Biotope",
#        y = "Estimated Richness")
# 
# # Afficher le graphique
# print(p)

```

```{r}
# Assurez-vous que vous avez les bibliothèques nécessaires
library(phyloseq)
library(ggplot2)
# 
# # Supposons que df_final est un objet phyloseq
# # Récupérer les données de l'objet phyloseq et ajouter les colonnes nécessaires
# sample_data_df <- as.data.frame(sample_data(df_final))
# sample_data_df$Estimated_richness <- summary(breakaway_richness)$estimate
# sample_data_df$Lower_CI <- summary(breakaway_richness)$lower
# sample_data_df$Upper_CI <- summary(breakaway_richness)$upper
# 
# # Créer le graphique avec ggplot2
# p <- ggplot(sample_data_df, aes(x=Biotope, y=Estimated_richness, color = Biotope)) + 
#   geom_boxplot() +
#   geom_errorbar(aes(ymin = Lower_CI, ymax = Upper_CI), width = 0.2) +
#   theme_minimal() + 
#   labs(title = "Estimated Richness with Confidence Intervals",
#        x = "Biotope",
#        y = "Estimated Richness")
# 
# # Afficher le graphique
# print(p)

```

```{r,  fig.width=20, fig.height=25}
library(ggpubr)
ggarrange(p, pr, nrow = 2, ncol=1) +  
         theme_bw()  + # Ajouter un thème 
         theme(legend.key.size = unit(2, "lines"),
              legend.text = element_text(size = 20),
              legend.title = element_text(size = 20)) +
         theme(axis.text.x = element_text(size = 10, hjust = 0.5))
```

#### ANOVA

We can now quantify this with an ANOVA.

```{r}
er <- estimate_richness(df_final_rarefied, measures = c("Observed", "Chao1", "Shannon", "InvSimpson"))
er[1:10,]
```

```{r}
data_break <- data.frame(sample_data(df_final))
data_break$Estimated_richness <- as.numeric(data_break$Estimated_richness)
data_break$Biotope <- as.factor(data_break$Biotope)
head(data_break)
```

```{r}
# we use aov() to fit an Analysis of Variance Model
model <- aov(Estimated_richness ~ Biotope, 
             data = data_break)

# and with anova() we compute analysis of variance tables for one or more fitted model objects.
anova(model) 
```

```{r}
div_data <- cbind(er,   # diversity indices
                  sample_data(df_final_rarefied)          # covariates 
                  )

# div_data
```

Now we do the ANOVA to check whether the means of the groups formed by our modalities are identical or not.

```{r}
# we use aov() to fit an Analysis of Variance Model
model <- aov(Observed ~ Biotope, 
             data = div_data)

# and with anova() we compute analysis of variance tables for one or more fitted model objects.
anova(model) 
```

The results clearly show that our data differ significantly in terms of the number of ASVs observed. Let's look with our other measurements.

```{r}
model <- aov(Chao1 ~ Biotope, data = div_data)
anova(model)
```

```{r}
model <- aov(Shannon ~ Biotope, data = div_data)
anova(model)
```

```{r}
model <- aov(InvSimpson ~ Biotope, data = div_data)
anova(model)
```

This is still the case with our other measures.

We therefore have the presence of diversity depending on our Biotope. And this is true on data that comes from different projects not related to each other.

:::

### Location

::: panel-tabset
#### Plots

Here we look at the diversity present at our different locations
```{r, fig.width=18, fig.height=10}
plot_richness(physeq = df_final_rarefied, 
              x = "Location",              # variable to map to the horizontal axis
              color = "Location",
              measures = c("Observed", "Chao1", "Shannon", "InvSimpson")) + 
  geom_boxplot(aes(group = Location))   +
  theme(legend.key.size = unit(2, "lines"),
        legend.text = element_text(size = 20),
        legend.title = element_text(size = 20)) +
    theme(axis.text.x = element_text(size = 10, hjust = 0.5))
 
```

We see a big difference depending on the location. But we must not forget that certain locations are well represented unlike others.

```{r}
table(sample_data(df_final_rarefied)$Location)
```

It does not seem relevant to fully consider locations which have a very low number of samples????

If we only focus on the highly represented locations.

```{r}
ll <- c("Belgium", "France", "France : Nyons", "France: Antony", "Germany", "USA", "Canada", "Australia", "France: Jouy-en-Josas", "France:Epoisses",  "France:Ile de France" )
sb_samp <- subset(sample_data(df_final_rarefied), sample_data(df_final_rarefied)$Location %in% ll)
samp = sb_samp
tax = tax_table(df_final_rarefied)%>% as.matrix()
otu = otu_table(df_final_rarefied)%>% as.matrix()

sb <- phyloseq(otu_table(otu, taxa_are_rows = TRUE),
                  tax_table(tax), 
                  sample_data(samp))
df_final_rarefied
sb
```

```{r, fig.width=18, fig.height=10}
plot_richness(physeq = sb, 
              x = "Location",              # variable to map to the horizontal axis
              color = "Location",
              measures = c("Observed", "Chao1", "Shannon", "InvSimpson")) + 
  geom_boxplot(aes(group = Location))   +
  theme(legend.key.size = unit(2, "lines"),
        legend.text = element_text(size = 20),
        legend.title = element_text(size = 20)) +
    theme(axis.text.x = element_text(size = 20, hjust = 0.5))
```

Here we can see a difference in diversity.

#### ANOVA

We can now quantify this with an ANOVA.

```{r}
er <- estimate_richness(sb, measures = c("Observed", "Chao1", "Shannon", "InvSimpson"))
er[1:10,]
```

```{r}
div_data <- cbind(er,   # diversity indices
                  sample_data(sb)          # covariates 
                  )
```

Now we do the ANOVA to check whether the means of the groups formed by our modalities are identical or not.

```{r}
# we use aov() to fit an Analysis of Variance Model
model <- aov(Observed ~ Location, 
             data = div_data)

# and with anova() we compute analysis of variance tables for one or more fitted model objects.
anova(model) 
```

The results clearly show that our data differ significantly in terms of the number of ASVs observed. Let's look with our other measurements.

```{r}
model <- aov(Chao1 ~ Location, data = div_data)
anova(model)
```

```{r}
model <- aov(Shannon ~ Location, data = div_data)
anova(model)
```

```{r}
model <- aov(InvSimpson ~ Location, data = div_data)
anova(model)
```

This is still the case with our other measures.

:::

### Source

::: panel-tabset
#### Plots

```{r}
table(sample_data(df_final_rarefied)$Source)
print(paste("Nombre de NA:", dim(sample_data(df_final_rarefied))[1] - length(na.omit(sample_data(df_final_rarefied)$Source)), ". Nb de non NA:",length(na.omit(sample_data(df_final_rarefied)$Source))))
```

```{r, fig.width=28, fig.height=10}
plot_richness(physeq = df_final_rarefied, 
              x = "Source",              # variable to map to the horizontal axis
              color = "Source",
              measures = c("Observed", "Chao1", "Shannon", "InvSimpson")) + 
  geom_boxplot(aes(group = Source)) +
  theme(legend.key.size = unit(2, "lines"),
        legend.text = element_text(size = 20),
        legend.title = element_text(size = 20)) +
    theme(axis.text.x = element_text(size = 20, hjust = 0.5))
```

#### ANOVA

We can now quantify this with an ANOVA.

```{r}
er <- estimate_richness(df_final_rarefied, measures = c("Observed", "Chao1", "Shannon", "InvSimpson"))
er[1:10,]
```

```{r}
div_data <- cbind(er,   # diversity indices
                  sample_data(df_final_rarefied)          # covariates 
                  )
```

Now we do the ANOVA to check whether the means of the groups formed by our modalities are identical or not.

```{r}
# we use aov() to fit an Analysis of Variance Model
model <- aov(Observed ~ Source, 
             data = div_data)

# and with anova() we compute analysis of variance tables for one or more fitted model objects.
anova(model) 
```

The results clearly show that our data differ significantly in terms of the number of ASVs observed. Let's look with our other measurements.

```{r}
model <- aov(Chao1 ~ Source, data = div_data)
anova(model)
```

```{r}
model <- aov(Shannon ~ Source, data = div_data)
anova(model)
```

```{r}
model <- aov(InvSimpson ~ Source, data = div_data)
anova(model)
```

This is still the case with our other measures.

:::

### PRJN

::: panel-tabset
#### Plots

We also look at the richness according to the different projects

```{r, fig.width=28, fig.height=10, warning=FALSE}
plot_richness(physeq = df_final_rarefied, 
              x = "PRJN",              # variable to map to the horizontal axis
              color = "PRJN",
              measures = c("Observed", "Chao1", "Shannon", "InvSimpson")) + 
  geom_boxplot(aes(group = PRJN)) +
  theme(legend.key.size = unit(2, "lines"),
        legend.text = element_text(size = 20),
        legend.title = element_text(size = 20)) +
    theme(axis.text.x = element_text(size = 20, hjust = 0.5))
```

#### ANOVA

On peut maintenant quantifier cela avec une ANOVA.

```{r}
er <- estimate_richness(df_final_rarefied, measures = c("Observed", "Chao1", "Shannon", "InvSimpson"))
er[1:10,]
```

```{r}
div_data <- cbind(er,   # diversity indices
                  sample_data(df_final_rarefied)          # covariates 
                  )
```

Now we do the ANOVA to check whether the means of the groups formed by our modalities are identical or not.

```{r}
# we use aov() to fit an Analysis of Variance Model
model <- aov(Observed ~ PRJN, 
             data = div_data)

# and with anova() we compute analysis of variance tables for one or more fitted model objects.
anova(model) 
```

The results clearly show that our data differ significantly in terms of the number of ASVs observed. Let's look with our other measurements.

```{r}
model <- aov(Chao1 ~ PRJN, data = div_data)
anova(model)
```

```{r}
model <- aov(Shannon ~ PRJN, data = div_data)
anova(model)
```

```{r}
model <- aov(InvSimpson ~ PRJN, data = div_data)
anova(model)
```

This is still the case with our other measures.

Afterwards, it may be interesting to put these results into perspective with the number of samples per study (do studies with many samples have more diversity? If so, how remarkable is this, is there studies with high diversity and few samples?). Same for our other variables.

:::

### Blend

```{r, fig.width=28, fig.height=10, warning=FALSE}
par(mfrow=c(1,2))
plot_richness(physeq = df_final_rarefied, 
              x = "Source",              # variable to map to the horizontal axis
              color = "Biotope",
              measures = c("Observed", "Chao1", "Shannon", "InvSimpson")) +
  theme(legend.key.size = unit(2, "lines"),
        legend.text = element_text(size = 20),
        legend.title = element_text(size = 20))
plot_richness(physeq = df_final_rarefied, 
              x = "PRJN",              # variable to map to the horizontal axis
              color = "Biotope",
              measures = c("Observed", "Chao1", "Shannon", "InvSimpson")) +
    theme(axis.text.x = element_text(size = 20, hjust = 0.5)) +
  theme(legend.key.size = unit(2, "lines"),
        legend.text = element_text(size = 20),
        legend.title = element_text(size = 20))
par(mfrow=c(1,1))
```

What we can see is that, whatever the project and whatever the source, we always have a strong diversity of species in the samples linked to the digest modality of our Biotope variable.

### Facet grid

We look at which sources contribute the most to one of the modalities of the Biotope variable.

```{r, fig.width=35, fig.height=15}
plot_richness(physeq = df_final_rarefied, 
              x = "Source",              # variable to map to the horizontal axis
              color = "Source",
              measures = c("Observed")) +
  facet_grid(. ~ Biotope, scales = "free_x", space = "free_x") +  
  geom_boxplot(aes(group = Source, fill = Source), color = "black", alpha = 0.4, outlier.shape = NA) +  # Add boxplot, hide outliers
  geom_jitter(width = 0.2, size = 1.5, alpha = 0.3) +  # Add points
  # theme_classic() +  # Set background to white
  theme(legend.key.size = unit(2, "lines"),
        legend.text = element_text(size = 20),
        legend.title = element_text(size = 20)) +
  theme(axis.text.x = element_text(size = 10, hjust = 0.5)) +
  labs(y = "Alpha Diversity Measures") +  
  labs(x = "") +  
  theme_bw() +
  theme(
    legend.key.size = unit(2, "lines"),
    legend.text = element_text(size = 25),
    legend.title = element_text(size = 35),
    legend.position="top",
    axis.text.x = element_text(size = 25, angle = 50, hjust = 1), 
    axis.title.y = element_blank(),
    axis.text.y = element_text(size = 30), 
    plot.title = element_text(size = 55, hjust = 0.5, face = "bold", margin = margin(t = 10, b = 10)),
    strip.text = element_text(size = 30)  # Augmenter la taille du texte des titres des sous-graphes
  )
 
```

```{r, fig.width=35, fig.height=15}
plot_richness(physeq = df_final_rarefied, 
              x = "organism_name",              # variable to map to the horizontal axis
              color = "organism_name",
              measures = c("Observed")) +
  facet_grid(. ~ Biotope, scales = "free_x", space = "free_x") +  
  geom_boxplot(aes(group = organism_name, fill = organism_name), color = "black", alpha = 0.4, outlier.shape = NA) +  # Add boxplot, hide outliers
  geom_jitter(width = 0.2, size = 1.5, alpha = 0.3) +  # Add points
  # theme_classic() +  # Set background to white
  theme(legend.key.size = unit(2, "lines"),
        legend.text = element_text(size = 20),
        legend.title = element_text(size = 20)) +
  theme(axis.text.x = element_text(size = 10, hjust = 0.5)) +
  labs(y = "Alpha Diversity Measures") +  
  labs(x = "") +  
  theme_bw() +
  theme(
    legend.key.size = unit(2, "lines"),
    legend.text = element_text(size = 25),
    legend.title = element_text(size = 35),
    legend.position="top",
    axis.text.x = element_text(size = 25, angle = 50, hjust = 1), 
    axis.title.y = element_blank(),
    axis.text.y = element_text(size = 30), 
    plot.title = element_text(size = 55, hjust = 0.5, face = "bold", margin = margin(t = 10, b = 10)),
    strip.text = element_text(size = 30)  # Augmenter la taille du texte des titres des sous-graphes
  )
 
```

```{r, fig.width=25, fig.height=10}
# plot alpha diversity

#BiocManager::install("microbiome")
#library(microbiome)
p.alphadiversity <- microbiome::boxplot_alpha(df_final_rarefied,
                           index = c("Observed"),
                           x_var = "Source",
                           )
p.alphadiversity +
    facet_grid(cols = vars(Biotope), scales = "free_x", space = "free_x") +
    theme(axis.text.x = element_text(size = 20, angle = 45, hjust = 1)) +
    theme(legend.position="top")+
  theme(legend.key.size = unit(2, "lines"),
        legend.text = element_text(size = 20),
        legend.title = element_text(size = 20))
```

We look with the variable organism_name which is a more detailed version of Biotope.

```{r, fig.width=25, fig.height=10}
# plot alpha diversity
p.alphadiversity <- microbiome::boxplot_alpha(df_final_rarefied,
                           index = c("Observed"),
                           x_var = "organism_name",
                           )
p.alphadiversity +
    facet_grid(cols = vars(Biotope), scales = "free_x", space = "free_x") +
    theme(axis.text.x = element_text(size = 20, angle = 45, hjust = 1)) +
    theme(axis.text.y = element_text(size = 20)) +
    theme(legend.position="top")+
  theme(legend.key.size = unit(1, "lines"),
        legend.text = element_text(size = 20),
        legend.title = element_text(size = 20))
```
:::

## Beta-Diversity

The $\beta-$diversity represents the compositional difference between samples (inter-sample diversity).

Here we do not have access to the phylogenetic tree so we cannot use the following distances:

-     UniFrac (Phylogenetic)

-     wUniFrac (Phylogenetic)

On the other hand, we can focus on the distances:

-     Jaccard (Compositional)

-     Bray-Curtis (Compositional)



```{r}
dist.jac <- distance(df_final_rarefied, method = "cc")
dist.bc <- distance(df_final_rarefied, method = "bray")
```

### Unconstrained ordination

We start by doing unconstrained ordination for these distances and then, when the variable has no NA, we can perform a PERMANOVA to test its effect.

::: panel-tabset
### Biotope

::: panel-tabset
#### Plots

```{r, fig.width=20, fig.height=8}
p.jac <- plot_ordination(df_final_rarefied, 
                         ordinate(df_final_rarefied, method = "MDS", distance = dist.jac), 
                         color = "Biotope") + ggtitle("Jaccard") 

p.bc <- plot_ordination(df_final_rarefied, 
                         ordinate(df_final_rarefied, method = "MDS", distance = dist.bc), 
                         color = "Biotope") + ggtitle("Bray-Curtis") 

```

```{r, fig.width=5, fig.height=7.5}
p.jac <- p.jac + 
  theme_bw() +
  theme(
    legend.position = "none",
    legend.key.size = unit(2, "lines"),
    legend.text = element_text(size = 40),
    legend.title = element_text(size = 50),
    axis.text.x =  element_text(size = 10, hjust = 0.5), 
    axis.title.y = element_text(size = 10, hjust = 0.5),
    plot.title = element_text(size = 35, hjust = 0.5, face = "bold", margin = margin(t = 10, b = 10))
  ) +
  guides(color = guide_legend(override.aes = list(size = 6))) 

p.jac

# gridExtra::grid.arrange(p.jac, p.bc, ncol = 2)
```

```{r, fig.width=7.5, fig.height=7.5}

p.bc <- p.bc + 
  theme_bw() +
  theme(
    # legend.position = "top",
    legend.key.size = unit(2, "lines"),
    legend.text = element_text(size = 20),
    legend.title = element_text(size = 25),
    axis.text.x =  element_text(size = 10, hjust = 0.5), 
    axis.title.y = element_text(size = 10, hjust = 0.5),
    plot.title = element_text(size = 35, hjust = 0.5, face = "bold", margin = margin(t = 10, b = 10))
  ) +
  guides(color = guide_legend(override.aes = list(size = 6)))

p.bc
# gridExtra::grid.arrange(p.jac, p.bc, ncol = 2)
```

#### PERMANOVA

```{r}
metadata <- sample_data(df_final_rarefied) %>% as("data.frame")
model <- vegan::adonis2(dist.bc ~Biotope , data = metadata, permutations = 99)
model
```

```{r}
metadata <- sample_data(df_final_rarefied) %>% as("data.frame")
model <- vegan::adonis2(dist.jac ~Biotope , data = metadata, permutations = 99)
model
```
:::

### Source

```{r, fig.width=20, fig.height=8}
p.jac_s<- plot_ordination(df_final_rarefied, 
                         ordinate(df_final_rarefied, method = "MDS", distance = dist.jac), 
                         color = "Source") 

p.bc_s <- plot_ordination(df_final_rarefied, 
                         ordinate(df_final_rarefied, method = "MDS", distance = dist.bc), 
                         color = "Source") 

gridExtra::grid.arrange(p.jac_s, p.bc_s, ncol = 2)
```

```{r, fig.width=5, fig.height=7.5}
# p.jac_s <- p.jac_s + 
#   theme_bw() +
#   theme(
#     legend.position = "none",
#     legend.key.size = unit(2, "lines"),
#     legend.text = element_text(size = 40),
#     legend.title = element_text(size = 50),
#     axis.text.x =  element_text(size = 10, hjust = 0.5), 
#     axis.title.y = element_text(size = 10, hjust = 0.5),
#     plot.title = element_text(size = 35, hjust = 0.5, face = "bold", margin = margin(t = 10, b = 10))
#   ) +
#   guides(color = guide_legend(override.aes = list(size = 6))) 
# 
# p.jac_s

# gridExtra::grid.arrange(p.jac, p.bc, ncol = 2)
```

```{r, fig.width=9.5, fig.height=9.5}

# p.bc_s <- p.bc_s + 
#   theme_bw() +
#   theme(
#     # legend.position = "top",
#     legend.key.size = unit(2, "lines"),
#     legend.text = element_text(size = 20),
#     legend.title = element_text(size = 25),
#     axis.text.x =  element_text(size = 10, hjust = 0.5), 
#     axis.title.y = element_text(size = 10, hjust = 0.5),
#     plot.title = element_text(size = 35, hjust = 0.5, face = "bold", margin = margin(t = 10, b = 10))
#   ) +
#   guides(color = guide_legend(override.aes = list(size = 6)))
# 
# p.bc_s
# gridExtra::grid.arrange(p.jac, p.bc, ncol = 2)
```

### Location

```{r, fig.width=28, fig.height=8}
p.jac <- plot_ordination(df_final_rarefied, 
                         ordinate(df_final_rarefied, method = "MDS", distance = dist.jac), 
                         color = "Location") + 
  ggtitle("Jaccard") +
  theme(legend.key.size = unit(2, "lines"),
        legend.text = element_text(size = 25),
        legend.title = element_text(size = 20))

p.bc <- plot_ordination(df_final_rarefied, 
                         ordinate(df_final_rarefied, method = "MDS", distance = dist.bc), 
                         color = "Location") + 
  ggtitle("Bray-Curtis") +
  theme(legend.key.size = unit(2, "lines"),
        legend.text = element_text(size = 25),
        legend.title = element_text(size = 20))

gridExtra::grid.arrange(p.jac, p.bc, ncol = 2)
```

### Organism name

```{r, fig.width=20, fig.height=8}
p.jac <- plot_ordination(df_final_rarefied, 
                         ordinate(df_final_rarefied, method = "MDS", distance = dist.jac), 
                         color = "organism_name") + 
  ggtitle("Jaccard") +
  theme(legend.key.size = unit(2, "lines"),
        legend.text = element_text(size = 20),
        legend.title = element_text(size = 20))

p.bc <- plot_ordination(df_final_rarefied, 
                         ordinate(df_final_rarefied, method = "MDS", distance = dist.bc), 
                         color = "organism_name") + 
  ggtitle("Bray-Curtis") +
  theme(legend.key.size = unit(2, "lines"),
        legend.text = element_text(size = 20),
        legend.title = element_text(size = 20))

gridExtra::grid.arrange(p.jac, p.bc, ncol = 2)
```
:::

## Hierarchical clustering

Il est aussi possible d'effectuer un clustering hierarchique à partir des indices de distance/dissimilarité de Jaccard et Bray-Curtis.

::: panel-tabset
### Biotope

```{r, message=FALSE, warning=FALSE}
plot_clust(df_final_rarefied, dist = "bray", method = "ward.D2", color = "Biotope", 
           title = "Clustering of samples (Bray-Curtis + Ward)\nsamples colored by Biotope")
```

binary = jaccard

```{r, message=FALSE, warning=FALSE}
plot_clust(df_final_rarefied, dist = "binary", method = "ward.D2", color = "Biotope", 
           title = "Clustering of samples (Jaccard + Ward)\nsamples colored by Biotope")
```

### Source

```{r, message=FALSE, warning=FALSE}
plot_clust(df_final_rarefied, dist = "bray", method = "ward.D2", color = "Source", 
           title = "Clustering of samples (Bray-Curtis + Ward)\nsamples colored by Biotope")
```

```{r, message=FALSE, warning=FALSE}
plot_clust(df_final_rarefied, dist = "binary", method = "ward.D2", color = "Source", 
           title = "Clustering of samples (Jaccard + Ward)\nsamples colored by Biotope")
```

### Location

```{r, message=FALSE, warning=FALSE}
plot_clust(df_final_rarefied, dist = "bray", method = "ward.D2", color = "Location", 
           title = "Clustering of samples (Bray-Curtis + Ward)\nsamples colored by Biotope")
```

```{r, message=FALSE, warning=FALSE}
plot_clust(df_final_rarefied, dist = "binary", method = "ward.D2", color = "Location", 
           title = "Clustering of samples (Jaccard + Ward)\nsamples colored by Biotope")
```

### organism_name

```{r, message=FALSE, warning=FALSE}
plot_clust(df_final_rarefied, dist = "bray", method = "ward.D2", color = "organism_name", 
           title = "Clustering of samples (Bray-Curtis + Ward)\nsamples colored by Biotope")
```

```{r, message=FALSE, warning=FALSE}
plot_clust(df_final_rarefied, dist = "binary", method = "ward.D2", color = "organism_name", 
           title = "Clustering of samples (Jaccard + Ward)\nsamples colored by Biotope")
```
:::
