---
title: "2_Open16s_Upset_analyzes"
author: "Clément Poupelin"
date: "2024-05-16"
format: 
  html:
    embed-resources: false
    toc: true
    code-fold: true
    code-summary: "Show the code"
    code-tools: true
    toc-location: right
    page-layout: article
    code-overflow: wrap
toc: true
number-sections: false
editor: visual
categories: [code, analysis]
---

Analyse des espèces communes à des modalités pour les données du projet Open16s de l'INRAe.
```{r, message=FALSE, warning=FALSE, echo=FALSE}
library(tidyverse)      ## data manipulation
library(phyloseq)       ## analysis of microbiome census data
library(ComplexHeatmap) ## UpsetPlot
Op16s_sp = readRDS("~/Documents/open16s/Data/df_final.rds")
dd <- t(merge_samples(otu_table(Op16s_sp),
                    group = sample_data(Op16s_sp)$Biotope))

matrice_binaire <- matrix(0, nrow = nrow(dd), ncol = 4)
dd <- as.data.frame(dd)
for (i in 1:ncol(dd)) {
  matrice_binaire[, i] <- as.integer(dd[, i] != 0)
}
matrice_binaire <- as.data.frame(matrice_binaire)
colnames(matrice_binaire) <- colnames(dd)
rownames(matrice_binaire) <- rownames(dd)

mc <- make_comb_mat(matrice_binaire)
ss = set_size(mc)
cs_biotope = comb_size(mc)
ht_biotope = UpSet(mc, 
    set_order = order(ss),
    comb_order = order(comb_degree(mc), -cs_biotope),
    top_annotation = HeatmapAnnotation(
        "Biotope Intersections" = anno_barplot(cs_biotope, 
            ylim = c(0, max(cs_biotope)*1.1),
            border = FALSE, 
            gp = gpar(fill = "black"), 
            height = unit(4, "cm")
        ), 
        annotation_name_side = "left", 
        annotation_name_rot = 90),
    left_annotation = rowAnnotation(
        "ASV by Biotope" = anno_barplot(-ss, 
            baseline = 0,
            axis_param = list(
                at = c(0, -500, -1000, -1500),
                labels = c(0, 500, 1000, 1500),
                labels_rot = 0),
            border = FALSE, 
            gp = gpar(fill = "black"), 
            width = unit(4, "cm")
        ),
        set_name = anno_text(set_name(mc), 
            location = 0.5, 
            just = "center",
            width = max_text_width(set_name(mc)) + unit(4, "mm"))
    ), 
    right_annotation = NULL,
    show_row_names = FALSE)
ht_biotope = draw(ht_biotope)
od_biotope = column_order(ht_biotope)
decorate_annotation("Biotope Intersections", {
    grid.text(cs_biotope[od_biotope], x = seq_along(cs_biotope), y = unit(cs_biotope[od_biotope], "native") + unit(2, "pt"), 
        default.units = "native", just = c("left", "bottom"), 
        gp = gpar(fontsize = 6, col = "#404040"), rot = 45)
})
```


## Introduction

Dans ce document nous aurons pour objectif d'étudier les interactions présentent au sein des différentes modalités des variables de nos données Open16s.

## Définition

Pour étudier les interactions présentent au sein des différentes modalités de nos variables, nous utiliserons des Upset plot. Ceux ci permettent de visualiser les intersections entre les ensembles dans une matrice et sont une alternatives au diagramme de Venn qui peut rapidement être limité pour des données de grande dimension.

[![UpSet_Intersection_Concept from wikipedia](UpSet_Intersection_Concept.svg.png){width="600" height="500" fig-align="left"}](https://en.wikipedia.org/wiki/UpSet_plot#/media/File:UpSet_Intersection_Concept.svg)

Ces plot peuvent ainsi être utilisés dans de nombreux autres domaines. On peut par exemple créer un Upset plot montrant comment les genres de films se croisent. [![Upset_Plot from wikipedia](Upset_Plot.png){width="600" height="500" fig-align="left"}](https://en.wikipedia.org/wiki/UpSet_plot#/media/File:Upset_Plot.png)

Ici, la plus grande intersection serait Comedy-Drama.

Dans le cas de données microbiologique agrégées par espèces, les upset plot nous permettrons de visualiser :

-   Combien d'espèces sont communes à différentes modalités d'une variable d'intérêt

-   Combien d'espèces sont uniques à différentes modalités d'une variable d'intérêt

-   Combien d'espèces sont présentent dans les différentes modalités d'une variable d'intérêt

## Etude de nos données

### Setup

On commence par charger les packages qui nous serons nécessaire pour notre étude.

```{r, message=FALSE}
library(tidyverse)      ## data manipulation
library(phyloseq)       ## analysis of microbiome census data
library(ComplexHeatmap) ## UpsetPlot
library(DT)             ## datatable
```

### Data import

Ici, nous utilisons les données issues du projet Open16s sur lesquels des modifications ont déjà été faites. En effet, il s'agit de données où la table d'OTU a été agrégée par espèces. Et nous avons aussi des modifications qui ont été effectuées sur les métadonnées (se référer au document Open16s_0).

```{r}
Op16s_sp = readRDS("~/Documents/open16s/Data/df_final.rds")
Op16s_sp
```

### UpsetPlot

Dans cette partie, nous allons montrer les différents Upset plot pour nos variables d'intérêt. Nous pourrons donc obtenir les informations décrites précédemment dans le cas du projet Open16s.

A noter que la construction des matrices de combinaison. se trouvant à la base des Upset plot sera détaillée seulement une fois puisque la procédure rete la même pour nos autres variables d'intérêt.

::: panel-tabset
### Biotope

Commençons par anlayser notre première variable, composée de 4 modalites.

Pour construire les Upset plot, nous allons devoir transformer nos données en une matrice de combinaison. qui contiendra les présences ou abscence d'interactions entre les modalites de nos variable.

Tout d'abord, on construit une table avec en individus les différentes espèces de ma table d'OTU et en variable les échantillons en fonction de la variable Biotope.

```{r}
dd <- t(merge_samples(otu_table(Op16s_sp),
                    group = sample_data(Op16s_sp)$Biotope))
```

Ensuite, on traduit notre nouvelle table en une table binaire. C'est à dire que l'on mettra 0 si l'espèce n'est pas présente dans la modalité de Biotope et 1 sinon.

```{r, message=FALSE, warning=FALSE}
matrice_binaire <- matrix(0, nrow = nrow(dd), ncol = 4)
dd <- as.data.frame(dd)
# Colonnes de matrice_binaire correspondant à vos variables
for (i in 1:ncol(dd)) {
  # Assigner les valeurs correspondantes à matrice_binaire
  matrice_binaire[, i] <- as.integer(dd[, i] != 0)
}

# On transforme la matrice en dataframe
matrice_binaire <- as.data.frame(matrice_binaire)
colnames(matrice_binaire) <- colnames(dd)
rownames(matrice_binaire) <- rownames(dd)
```

On peut maintenant créer la matrice de combinaison qui sera à la base de nos Upset plot.

```{r}
mc <- make_comb_mat(matrice_binaire)
print(mc)
```

Puis on peut l'utiliser pour faire nos Upset plot.

```{r}
ss = set_size(mc)
cs_biotope = comb_size(mc)
ht_biotope = UpSet(mc, 
    set_order = order(ss),
    comb_order = order(comb_degree(mc), -cs_biotope),
    top_annotation = HeatmapAnnotation(
        "Biotope Intersections" = anno_barplot(cs_biotope, 
            ylim = c(0, max(cs_biotope)*1.1),
            border = FALSE, 
            gp = gpar(fill = "black"), 
            height = unit(4, "cm")
        ), 
        annotation_name_side = "left", 
        annotation_name_rot = 90),
    left_annotation = rowAnnotation(
        "ASV by Biotope" = anno_barplot(-ss, 
            baseline = 0,
            axis_param = list(
                at = c(0, -500, -1000, -1500),
                labels = c(0, 500, 1000, 1500),
                labels_rot = 0),
            border = FALSE, 
            gp = gpar(fill = "black"), 
            width = unit(4, "cm")
        ),
        set_name = anno_text(set_name(mc), 
            location = 0.5, 
            just = "center",
            width = max_text_width(set_name(mc)) + unit(4, "mm"))
    ), 
    right_annotation = NULL,
    show_row_names = FALSE)
ht_biotope = draw(ht_biotope)
od_biotope = column_order(ht_biotope)
decorate_annotation("Biotope Intersections", {
    grid.text(cs_biotope[od_biotope], x = seq_along(cs_biotope), y = unit(cs_biotope[od_biotope], "native") + unit(2, "pt"), 
        default.units = "native", just = c("left", "bottom"), 
        gp = gpar(fontsize = 6, col = "#404040"), rot = 45)
})
```

Ensuite, on peut subdiviser selon les interactions que l'on veut voir (double, triple, etc...)

```{r}
mc2 = mc[comb_degree(mc) == 2]
ss = set_size(mc2)
cs = comb_size(mc2)
ht = UpSet(mc2, 
    set_order = order(ss),
    comb_order = order(comb_degree(mc2), -cs),
    top_annotation = HeatmapAnnotation(
        "Biotope Intersections" = anno_barplot(cs, 
            ylim = c(0, max(cs)*1.1),
            border = FALSE, 
            gp = gpar(fill = "black"), 
            height = unit(4, "cm")
        ), 
        annotation_name_side = "left", 
        annotation_name_rot = 90),
    left_annotation = rowAnnotation(
        "ASV by Biotope" = anno_barplot(-ss, 
            baseline = 0,
            axis_param = list(
                at = c(0, -500, -1000, -1500),
                labels = c(0, 500, 1000, 1500),
                labels_rot = 0),
            border = FALSE, 
            gp = gpar(fill = "black"), 
            width = unit(4, "cm")
        ),
        set_name = anno_text(set_name(mc2), 
            location = 0.5, 
            just = "center",
            width = max_text_width(set_name(mc2)) + unit(4, "mm"))
    ), 
    right_annotation = NULL,
    show_row_names = FALSE)
ht = draw(ht)
od = column_order(ht)
decorate_annotation("Biotope Intersections", {
    grid.text(cs[od], x = seq_along(cs), y = unit(cs[od], "native") + unit(2, "pt"), 
        default.units = "native", just = c("left", "bottom"), 
        gp = gpar(fontsize = 6, col = "#404040"), rot = 45)
})
```

```{r}
mc3 = mc[comb_degree(mc) == 3]
ss = set_size(mc3)
cs = comb_size(mc3)
ht = UpSet(mc3, 
    set_order = order(ss),
    comb_order = order(comb_degree(mc3), -cs),
    top_annotation = HeatmapAnnotation(
        "Biotope Intersections" = anno_barplot(cs, 
            ylim = c(0, max(cs)*1.1),
            border = FALSE, 
            gp = gpar(fill = "black"), 
            height = unit(4, "cm")
        ), 
        annotation_name_side = "left", 
        annotation_name_rot = 90),
    left_annotation = rowAnnotation(
        "ASV by Biotope" = anno_barplot(-ss, 
            baseline = 0,
            axis_param = list(
                at = c(0, -500, -1000, -1500),
                labels = c(0, 500, 1000, 1500),
                labels_rot = 0),
            border = FALSE, 
            gp = gpar(fill = "black"), 
            width = unit(4, "cm")
        ),
        set_name = anno_text(set_name(mc3), 
            location = 0.5, 
            just = "center",
            width = max_text_width(set_name(mc3)) + unit(4, "mm"))
    ), 
    right_annotation = NULL,
    show_row_names = FALSE)
ht = draw(ht)
od = column_order(ht)
decorate_annotation("Biotope Intersections", {
    grid.text(cs[od], x = seq_along(cs), y = unit(cs[od], "native") + unit(2, "pt"), 
        default.units = "native", just = c("left", "bottom"), 
        gp = gpar(fontsize = 6, col = "#404040"), rot = 45)
})
```

### PRJN

Nous avons refait la même procédure pour notre variable PRJN.

```{r}
dd <- t(merge_samples(otu_table(Op16s_sp), 
                    group = sample_data(Op16s_sp)$PRJN))

matrice_binaire <- matrix(0, nrow = nrow(dd), ncol = ncol(dd))
dd<-as.data.frame(dd)
for (i in 1:ncol(dd)) {
  matrice_binaire[, i] <- as.integer(dd[, i] != 0)
}
matrice_binaire <- as.data.frame(matrice_binaire)
colnames(matrice_binaire) <- colnames(dd)
rownames(matrice_binaire) <- rownames(dd)

mc <- make_comb_mat(matrice_binaire)
```

```{r, fig.width=18, fig.height=8}
for( i in 1:15){
  mc1 <- mc[comb_degree(mc) == i]
  ss = set_size(mc1)
  cs = comb_size(mc1)
  ht = UpSet(mc1, 
    set_order = order(ss),
    comb_order = order(comb_degree(mc1), -cs),
    top_annotation = HeatmapAnnotation(
        "PRJN Intersections" = anno_barplot(cs, 
            ylim = c(0, max(cs)*1.1),
            border = FALSE, 
            gp = gpar(fill = "black"), 
            height = unit(4, "cm")
        ), 
        annotation_name_side = "left", 
        annotation_name_rot = 90),
    left_annotation = rowAnnotation(
        "ASV by PRJN" = anno_barplot(-ss, 
            baseline = 0,
            axis_param = list(
                at = c(0, -500, -1000, -1500),
                labels = c(0, 500, 1000, 1500),
                labels_rot = 0),
            border = FALSE, 
            gp = gpar(fill = "black"), 
            width = unit(4, "cm")
        ),
        set_name = anno_text(set_name(mc1), 
            location = 0.5, 
            just = "center",
            width = max_text_width(set_name(mc1)) + unit(4, "mm"))
    ), 
    right_annotation = NULL,
    show_row_names = FALSE)
  ht = draw(ht)
  od = column_order(ht)
  decorate_annotation("PRJN Intersections", {
    grid.text(cs[od], x = seq_along(cs), y = unit(cs[od], "native") + unit(2, "pt"), 
        default.units = "native", just = c("left", "bottom"), 
        gp = gpar(fontsize = 6, col = "#404040"), rot = 45)
})
}

```

A noter qu'il n'y a pas d'espèces communes à 16 ou 17 modalités de notre variable PRJN.

### Location

Nous avons refait la même procédure pour notre variable Location.

```{r, warning=FALSE}
dd <- t(merge_samples(otu_table(Op16s_sp), 
                    group = sample_data(Op16s_sp)$Location))

matrice_binaire <- matrix(0, nrow = nrow(dd), ncol = ncol(dd))

dd<-as.data.frame(dd)

for (i in 1:ncol(dd)) {
  matrice_binaire[, i] <- as.integer(dd[, i] != 0)
}

matrice_binaire <- as.data.frame(matrice_binaire)

colnames(matrice_binaire) <- colnames(dd)
rownames(matrice_binaire) <- rownames(dd)
view(matrice_binaire)

mc <- make_comb_mat(na.omit(matrice_binaire))

```

```{r, fig.width=18, fig.height=8}
for( i in 1:11){
  mc1 <- mc[comb_degree(mc) == i]
  ss = set_size(mc1)
  cs = comb_size(mc1)
  ht = UpSet(mc1, 
    set_order = order(ss),
    comb_order = order(comb_degree(mc1), -cs),
    top_annotation = HeatmapAnnotation(
        "Location Intersections" = anno_barplot(cs, 
            ylim = c(0, max(cs)*1.1),
            border = FALSE, 
            gp = gpar(fill = "black"), 
            height = unit(4, "cm")
        ), 
        annotation_name_side = "left", 
        annotation_name_rot = 90),
    left_annotation = rowAnnotation(
        "ASV by Location" = anno_barplot(-ss, 
            baseline = 0,
            axis_param = list(
                at = c(0, -500, -1000, -1500),
                labels = c(0, 500, 1000, 1500),
                labels_rot = 0),
            border = FALSE, 
            gp = gpar(fill = "black"), 
            width = unit(4, "cm")
        ),
        set_name = anno_text(set_name(mc1), 
            location = 0.5, 
            just = "center",
            width = max_text_width(set_name(mc1)) + unit(4, "mm"))
    ), 
    right_annotation = NULL,
    show_row_names = FALSE)
  ht = draw(ht)
  od = column_order(ht)
  decorate_annotation("Location Intersections", {
    grid.text(cs[od], x = seq_along(cs), y = unit(cs[od], "native") + unit(2, "pt"), 
        default.units = "native", just = c("left", "bottom"), 
        gp = gpar(fontsize = 6, col = "#404040"), rot = 45)
})
}

```

A noter qu'il n'y a pas d'espèces communes à 12 ou plus de modalités de notre variable Location.

### Source

Nous avons refait la même procédure pour notre variable Source.

```{r, warning=FALSE}
dd <- t(merge_samples(otu_table(Op16s_sp), 
                    group = sample_data(Op16s_sp)$Source))


matrice_binaire <- matrix(0, nrow = nrow(dd), ncol = ncol(dd))

dd<-as.data.frame(dd)

for (i in 1:ncol(dd)) {
  # Assigner les valeurs correspondantes à matrice_binaire
  matrice_binaire[, i] <- as.integer(dd[, i] != c(0, NA))
}

matrice_binaire <- as.data.frame(matrice_binaire)
# Affichage de la matrice binaire

colnames(matrice_binaire) <- colnames(dd)
rownames(matrice_binaire) <- rownames(dd)
#view(matrice_binaire)
if(is.na(colnames(matrice_binaire)[ncol(dd)])==TRUE){
colnames(matrice_binaire)[ncol(dd)]<-"NA"
}

mc <- make_comb_mat(na.omit(matrice_binaire))
```

```{r, fig.width=18, fig.height=8}
for( i in 1:10){
  mc1 <- mc[comb_degree(mc) == i]
  ss = set_size(mc1)
  cs = comb_size(mc1)
  ht = UpSet(mc1, 
    set_order = order(ss),
    comb_order = order(comb_degree(mc1), -cs),
    top_annotation = HeatmapAnnotation(
        "Source Intersections" = anno_barplot(cs, 
            ylim = c(0, max(cs)*1.1),
            border = FALSE, 
            gp = gpar(fill = "black"), 
            height = unit(4, "cm")
        ), 
        annotation_name_side = "left", 
        annotation_name_rot = 90),
    left_annotation = rowAnnotation(
        "ASV by Source" = anno_barplot(-ss, 
            baseline = 0,
            axis_param = list(
                at = c(0, -500, -1000, -1500),
                labels = c(0, 500, 1000, 1500),
                labels_rot = 0),
            border = FALSE, 
            gp = gpar(fill = "black"), 
            width = unit(4, "cm")
        ),
        set_name = anno_text(set_name(mc1), 
            location = 0.5, 
            just = "center",
            width = max_text_width(set_name(mc1)) + unit(4, "mm"))
    ), 
    right_annotation = NULL,
    show_row_names = FALSE)
  ht = draw(ht)
  od = column_order(ht)
  decorate_annotation("Source Intersections", {
    grid.text(cs[od], x = seq_along(cs), y = unit(cs[od], "native") + unit(2, "pt"), 
        default.units = "native", just = c("left", "bottom"), 
        gp = gpar(fontsize = 6, col = "#404040"), rot = 45)
})
}

```

A noter qu'il n'y a pas d'espèces communes à 11 ou plus de modalités de notre variable Source.
:::

### Automatisation et extraction

A la vue des Upset plot présent ci dessus, on serait maintenant tenté de vouloir récupérer le nom des espèces communes à plusieurs modalités. Ne serait-ce que pour vérifier si ce sont des espèces correctement identifiées ou s'il s'agit de multi affiliation/ unknown species.

Pour cela, 2 fonctions ont été crées. Premièrement, une fonction à été faite pour faciliter la visualisation des Upset plot qui nous interesse. Puis une seconde va nous servir à extraire le nom des espèces. C'es deux fonctions se complètes et marchent en paire.

::: panel-tabset
### Function

on a les codes qui ont permis de faire les premier tests de la fonction

```{r, warning=FALSE}
variable <-"Source"
level <- 6
combinaison = "1110001100000001"
rang_taxonomique = 7
## On groupe selon la variable d'intérêt nos colonnes de l'otu_table
dd <- as.data.frame(t(merge_samples(otu_table(Op16s_sp), 
                    group = as.matrix(sample_data(Op16s_sp)[, variable]))))


## On transforme en matrice binaire 
matrice_binaire <- matrix(0, nrow = nrow(dd), ncol = ncol(dd))
for (i in 1:ncol(dd)) {
  # Assigner les valeurs correspondantes à matrice_binaire
  matrice_binaire[, i] <- as.integer(dd[, i] != 0)
}
matrice_binaire <- as.data.frame(matrice_binaire)
colnames(matrice_binaire) <- colnames(dd)
rownames(matrice_binaire) <- rownames(dd)


colnames(matrice_binaire)[ncol(dd)]<-"NA"

mc <- make_comb_mat(matrice_binaire)
sub_mc <- mc[comb_degree(mc) == level]

ls <- extract_comb(sub_mc, combinaison)


new_otu_table <- otu_table(Op16s_sp)[ls,]

samp = sample_data(Op16s_sp)
tax = tax_table(Op16s_sp)%>% as.matrix()
otu = new_otu_table%>% as.matrix()

df <- phyloseq(otu_table(otu, taxa_are_rows = TRUE),
                  tax_table(tax), 
                  sample_data(samp))

tt <- table(tax_table(df)[, rang_taxonomique]) 
nom_rang <- colnames(tax_table(df)[, rang_taxonomique])
liste <- list(name = names(tt), freq = as.vector(tt))

```

Une fois la procédure identifiée, il ne restait plus qu'a imlémenter dans une fonction.

```{r}
common_ASV <- function(data, variable, level, combinaison, rang_taxonomique) {


## On groupe selon la variable d'intérêt nos colonnes de l'otu_table
dd <- as.data.frame(t(merge_samples(otu_table(data), 
                    group = as.matrix(sample_data(data)[, variable]))))


## On transforme en matrice binaire 
matrice_binaire <- matrix(0, nrow = nrow(dd), ncol = ncol(dd))
for (i in 1:ncol(dd)) {
  # Assigner les valeurs correspondantes à matrice_binaire
  matrice_binaire[, i] <- as.integer(dd[, i] != 0)
}
matrice_binaire <- as.data.frame(matrice_binaire)
colnames(matrice_binaire) <- colnames(dd)
rownames(matrice_binaire) <- rownames(dd)

## Si on a des modalités NA dans la variable d'intérêt,
## on renomme manuellement la colonne en NA
if(is.na(colnames(matrice_binaire)[ncol(dd)])==TRUE){
colnames(matrice_binaire)[ncol(dd)]<-"NA"
}

## On fait notre matrice des combinaison
mc <- make_comb_mat(matrice_binaire)

## On subdivise la matrice en isolant la combinaison qui nous intéresse
sub_mc <- mc[comb_degree(mc) == level]

## On extrait les infos selon la combinaison de modalité voulue
ls <- extract_comb(sub_mc, combinaison)

## On fait une nouvelle table d'OTU pour construire un nouvel objet phyloseq
new_otu_table <- otu_table(data)[ls,]

samp = sample_data(data)
tax = tax_table(data) %>% as.matrix()
otu = new_otu_table %>% as.matrix()

df <- phyloseq(otu_table(otu, taxa_are_rows = TRUE),
                  tax_table(tax), 
                  sample_data(samp))

tt <- table(tax_table(df)[, rang_taxonomique]) 
nom_rang <- colnames(tax_table(df)[, rang_taxonomique])
liste <- list(name = names(tt), freq = as.vector(tt))

## On fait une table d'affichage
res_mat <- matrix(NA, ncol = 2, nrow = length(names(tt)))
res_mat[,1] <- names(tt)
res_mat[,2] <- as.vector(tt)
res_df <- as.data.frame(res_mat)
colnames(res_df) <- c("Name", "Freq")
df_table <- res_df %>% datatable


  return(as.list(x = list(name = liste$name, freq = liste$freq, rank = nom_rang, table = df_table)))
}


```

Puis, pour compléter on à la fonction pour les upst plot qui est la pour reproduire les plots vu précédemment de manière plus optimisé.
```{r}
sub_upsetplot = function(data, variable, level, ineg = FALSE){

## On groupe selon la variable d'intérêt nos colonnes de l'otu_table
dd <- as.data.frame(t(merge_samples(otu_table(data), 
                    group = as.matrix(sample_data(data)[, variable]))))


## On transforme en matrice binaire 
matrice_binaire <- matrix(0, nrow = nrow(dd), ncol = ncol(dd))
for (i in 1:ncol(dd)) {
  # Assigner les valeurs correspondantes à matrice_binaire
  matrice_binaire[, i] <- as.integer(dd[, i] != 0)
}
matrice_binaire <- as.data.frame(matrice_binaire)
colnames(matrice_binaire) <- colnames(dd)
rownames(matrice_binaire) <- rownames(dd)

## Si on a des modalités NA dans la variable d'intérêt,
## on renomme manuellement la colonne en NA
if(is.na(colnames(matrice_binaire)[ncol(dd)])==TRUE){
colnames(matrice_binaire)[ncol(dd)]<-"NA"
}

## On fait notre matrice des combinaison
mc <- make_comb_mat(na.omit(matrice_binaire))

## On subdivise la matrice en isolant la combinaison qui nous intéresse
## on commence par définir si on cherche une combi précise ou celle inf à un niveau
if(ineg == TRUE){
  sub_mc = mc[comb_degree(mc) <= level]
} else sub_mc = mc[comb_degree(mc) == level]

ss = set_size(sub_mc)
cs = comb_size(sub_mc)
ht = UpSet(sub_mc, 
    set_order = rownames(mc),
    comb_order = order(comb_degree(sub_mc), -cs),
    top_annotation = HeatmapAnnotation(
        "Intersections" = anno_barplot(cs, 
            ylim = c(0, max(cs)*1.1),
            border = FALSE, 
            gp = gpar(fill = "black"), 
            height = unit(4, "cm")
        ), 
        annotation_name_side = "left", 
        annotation_name_rot = 90),
    left_annotation = rowAnnotation(
        "ASV by components" = anno_barplot(-ss, 
            baseline = 0,
            axis_param = list(
                at = c(0, -500, -1000, -1500),
                labels = c(0, 500, 1000, 1500),
                labels_rot = 0),
            border = FALSE, 
            gp = gpar(fill = "black"), 
            width = unit(4, "cm")
        ),
        set_name = anno_text(set_name(sub_mc), 
            location = 0.5, 
            just = "center",
            width = max_text_width(set_name(sub_mc)) + unit(4, "mm"))
    ), 
    right_annotation = NULL,
    show_row_names = FALSE)
ht = draw(ht)
od = column_order(ht)
decorate_annotation("Intersections", {
    grid.text(cs[od], x = seq_along(cs), y = unit(cs[od], "native") + unit(2, "pt"), 
        default.units = "native", just = c("left", "bottom"), 
        gp = gpar(fontsize = 6, col = "#404040"), rot = 45)
})

}
```

### Utilisation

Pour comprendre comment utiliser nos fonctions, faisons via des exemples. Premièrement, il faudra compléter les informations suivantes :

-   **data**: on entre le jeux de données que l'on veut étudier

-   **variable** : on entre notre variable d'intérêt

-   **level** : on met le "degrée de combinaison" (par ex si on veut regarder le nombre d'espèces communes entre 2 biotope, le niveau est 2)

-   **combinaison** : on y met la combinaison binaire correspondant à ce que l'on souhaite (par exemple, pour les biotopes on peut regarder les espèces communes à cow et food avec le code binaire 1010)

-   **rang_taxonomique** : on y met le rang taxonomique en renseignant le numéro de la colonne qui correspond à ce rang (par exemple pour Species c'est la colonne 7 de notre tax_table)

Ici, nous allons donc commencer par regarder les espèces communes (rang_taxonomique=7) entre 3 biotopes (variable = "Biotope", level = 3). Pour cela on regarde sur l'UpsetPlot les biotopes qui nous interesse.

```{r}
sub_upsetplot(data = Op16s_sp, variable = "Biotope", level = 3)
```

Ainsi, pour regarder les espèces commune à cow, digester et food, le code serait 1110

```{r}
res <- common_ASV(data = Op16s_sp, variable = "Biotope", level = 3, combinaison = "1110", rang_taxonomique = 7)
print(paste("Rang taxonomique : ",res$rank))
res$name
```
Avel les fréquences, on peut retrouver le nombre d'espèces identifiées comme communes.
```{r}
sum(res$freq)
```

Et on peut afficher la table
```{r}
res$table
```

On peut aussi regardre les Upset plot avec des inégalités.
```{r}
sub_upsetplot(data = Op16s_sp, variable = "Biotope", level = 2, ineg=TRUE)
```

Regardons maintenant pour une autre variable avec plus de modalités.
```{r}
sub_upsetplot(data = Op16s_sp, variable = "Source", level = 3)
```

Ainsi, pour regarder les espèces commune à cow milk, cow_teat_skin et beer, le code serait 0100001100000000
```{r, warning=FALSE, message=FALSE}
res <- common_ASV(data = Op16s_sp, variable = "Source", level = 3, combinaison = "0100001100000000", rang_taxonomique = 7)
print(paste("Rang taxonomique : ",res$rank))
res$table
```

Essayons maintenant de visulaliser les espèces communes pour des "relations plus fortes"

```{r}
sub_upsetplot(data = Op16s_sp, variable = "Source", level = 6)
```

Ainsi, pour regarder les espèces commune à Batch anaerobic digester, Beer, biogas fermenter, cow milk, cow_teat_skins, NA, le code serait 1110001100000001

```{r, warning=FALSE, message=FALSE}
res <- common_ASV(data = Op16s_sp, variable = "Source", level = 6, combinaison = "1110001100000001", rang_taxonomique = 7)
print(paste("Rang taxonomique : ",res$rank))
res$table
```

On regarde les espèces communes pour Batch anaerobic digester, biogas fermenter, cow milk, cow_teat_skins, NA_Homo sapiens_Feces, NA

```{r, warning=FALSE, message=FALSE}
res <- common_ASV(data = Op16s_sp, variable = "Source", level = 6, combinaison = "1010001100010001", rang_taxonomique = 7)
print(paste("Rang taxonomique : ",res$rank))
res$table
```

A un autre rang taxonomique

```{r, warning=FALSE, message=FALSE}
res <- common_ASV(data = Op16s_sp, variable = "Source", level = 6, combinaison = "1010001100010001", rang_taxonomique = 4)
print(paste("Rang taxonomique : ",res$rank))
res$table
```
:::

### Analyse détailée du Biotope

Dans cette partie, puisque notre variable Biotope possède 4 modalités, on se propose de tout regarder.

```{r}
ht_biotope
decorate_annotation("Biotope Intersections", {
    grid.text(cs_biotope[od_biotope], x = seq_along(cs_biotope), y = unit(cs_biotope[od_biotope], "native") + unit(2, "pt"), 
        default.units = "native", just = c("left", "bottom"), 
        gp = gpar(fontsize = 6, col = "#404040"), rot = 45)
})
```

::: panel-tabset
### level 4

```{r}
sub_upsetplot(data = Op16s_sp, variable = "Biotope", level = 4)
```

On voit qu'il y a 41 espèces en commun

```{r}
res <- common_ASV(data = Op16s_sp, variable = "Biotope", level = 4, combinaison = "1111", rang_taxonomique = 7)
print(paste("Rang taxonomique : ",res$rank))
res$table
```

On a que cela correspond à des multi-affiliation et unkown species. Si on change le rang taxonomique

```{r}
res <- common_ASV(data = Op16s_sp, variable = "Biotope", level = 4, combinaison = "1111", rang_taxonomique = 6)
print(paste("Rang taxonomique : ",res$rank))
res$table
```

On peut potentiellement ici obtenir plus d'informations

### level 3

```{r}
sub_upsetplot(data = Op16s_sp, variable = "Biotope", level = 3)
```

#### cow, digester et food :

```{r}
res <- common_ASV(data = Op16s_sp, variable = "Biotope", level = 3, combinaison = "1110", rang_taxonomique = 7)
print(paste("Rang taxonomique : ",res$rank))
res$table
```

#### cow, food et human_gut :

```{r}
res <- common_ASV(data = Op16s_sp, variable = "Biotope", level = 3, combinaison = "1011", rang_taxonomique = 7)
print(paste("Rang taxonomique : ",res$rank))
res$table
```

On change de rang

```{r}
res <- common_ASV(data = Op16s_sp, variable = "Biotope", level = 3, combinaison = "1011", rang_taxonomique = 6)
print(paste("Rang taxonomique : ",res$rank))
res$table
```

#### cow, digester et human_gut :

```{r}
res <- common_ASV(data = Op16s_sp, variable = "Biotope", level = 3, combinaison = "1101", rang_taxonomique = 7)
print(paste("Rang taxonomique : ",res$rank))
res$table
```

On change de rang

```{r}
res <- common_ASV(data = Op16s_sp, variable = "Biotope", level = 3, combinaison = "1101", rang_taxonomique = 6)
print(paste("Rang taxonomique : ",res$rank))
res$table
```

#### digester, food et human_gut :

```{r}
res <- common_ASV(data = Op16s_sp, variable = "Biotope", level = 3, combinaison = "0111", rang_taxonomique = 7)
print(paste("Rang taxonomique : ",res$rank))
res$table
```

On change de rang

```{r}
res <- common_ASV(data = Op16s_sp, variable = "Biotope", level = 3, combinaison = "0111", rang_taxonomique = 6)
print(paste("Rang taxonomique : ",res$rank))
res$table
```

### level 2

```{r}
sub_upsetplot(data = Op16s_sp, variable = "Biotope", level = 2)
```

#### cow et food :

```{r}
res <- common_ASV(data = Op16s_sp, variable = "Biotope", level = 2, combinaison = "1010", rang_taxonomique = 7)
print(paste("Rang taxonomique : ",res$rank))
res$table
```

#### digester et food :

```{r}
res <- common_ASV(data = Op16s_sp, variable = "Biotope", level = 2, combinaison = "0110", rang_taxonomique = 7)
print(paste("Rang taxonomique : ",res$rank))
res$table
```

#### cow et digester :

```{r}
res <- common_ASV(data = Op16s_sp, variable = "Biotope", level = 2, combinaison = "1100", rang_taxonomique = 7)
print(paste("Rang taxonomique : ",res$rank))
res$table
```

#### food et human_gut :

```{r}
res <- common_ASV(data = Op16s_sp, variable = "Biotope", level = 2, combinaison = "0011", rang_taxonomique = 7)
print(paste("Rang taxonomique : ",res$rank))
res$table
```

#### digester et human_gut :

```{r}
res <- common_ASV(data = Op16s_sp, variable = "Biotope", level = 2, combinaison = "0101", rang_taxonomique = 7)
print(paste("Rang taxonomique : ",res$rank))
res$table
```

#### cow et human_gut :

```{r}
res <- common_ASV(data = Op16s_sp, variable = "Biotope", level = 2, combinaison = "1001", rang_taxonomique = 7)
print(paste("Rang taxonomique : ",res$rank))
res$table
```

### level 1

Ici on regarde donc les espèces qui ne sont pas communes.

```{r}
sub_upsetplot(data = Op16s_sp, variable = "Biotope", level = 1)
```

#### digester :

```{r}
res <- common_ASV(data = Op16s_sp, variable = "Biotope", level = 1, combinaison = "0100", rang_taxonomique = 7)
print(paste("Rang taxonomique : ",res$rank))
res$table
```

#### food :

```{r}
res <- common_ASV(data = Op16s_sp, variable = "Biotope", level = 1, combinaison = "0010", rang_taxonomique = 7)
print(paste("Rang taxonomique : ",res$rank))
res$table
```

#### cow :

```{r}
res <- common_ASV(data = Op16s_sp, variable = "Biotope", level = 1, combinaison = "1000", rang_taxonomique = 7)
print(paste("Rang taxonomique : ",res$rank))
res$table
```

#### human_gut :

```{r}
res <- common_ASV(data = Op16s_sp, variable = "Biotope", level = 1, combinaison = "0001", rang_taxonomique = 7)
print(paste("Rang taxonomique : ",res$rank))
res$table
```
:::
